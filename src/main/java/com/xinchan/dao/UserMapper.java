package com.xinchan.dao;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.xinchan.pojo.User;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.springframework.data.relational.core.sql.In;

//对应的mapper上继承基本的类  BaseMapper
@Mapper
public interface UserMapper extends BaseMapper<User> {
    //所有的CRUD操作都已经编写完成了
    User selectById(@Param("id") Integer id);
}
